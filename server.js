const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const mongoose = require('./config/database'); //database configuration
const users = require('./routes/users');
const pharmacy = require('./routes/pharmacy');
const medicine = require('./routes/medicine');
const inventory = require('./routes/inventory')
const noAuth = require('./routes/noAuth');

const app = express();

var jwt = require('jsonwebtoken');

//jwt secret token
app.set('secretKey', 'pharmacySvc');

//connection to mongoDB
mongoose.connection.on('error', console.error.bind(console, 'mongoDB connection error:'));

app.use(cors({
    exposedHeaders: ['X-Total-Count']
}));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/', function (req, res) {
    res.json({
        "tutorial": "Build REST API with node.js"
    });
});

//public route
app.use('/users', noAuth);

// private route
app.use('/users', validateUser, users);
app.use('/pharmacy', validateUser, pharmacy);
app.use('/medicine', validateUser, medicine);
app.use('/inventory', validateUser, inventory)

app.get('/favicon.ico', function (req, res) {
    res.sendStatus(204);
});

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            console.log(err.message)
            res.json({
                status: "error",
                message: err.message,
            });
        } else {
            // add user id to request
            req.body.userId = decoded.id;
            next();
        }
    });
}


// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// handle errors
app.use(function (err, req, res, next) {
    console.log(err);

    if (err.status === 404) {
        res.status(404).json({ message: "Not found" });
    } else {
        res.status(500).json({ message: "Internal server error" });
    }
});

app.listen(3001, function () {
    console.log('Node server listening on port 3001');
});