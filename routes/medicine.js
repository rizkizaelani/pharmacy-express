const express = require('express');
const router = express.Router();

const medicineController = require('../app/api/controllers/medicine');

router.post('/', medicineController.createMedicine);
router.get('/', medicineController.getAllMedicine);
router.get('/:medicineId', medicineController.getMedicineById);
router.put('/:medicineId', medicineController.updateMedicineById);
router.delete('/:medicineId', medicineController.deleteMedicineById);

module.exports = router;