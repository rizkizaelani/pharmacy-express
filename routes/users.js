const express = require('express');
const router = express.Router();

const userController = require('../app/api/controllers/users');

router.get('/', userController.getAllUser);
router.get('/:userId', userController.getUserById);
router.put('/:userId', userController.updateUserById);
router.delete('/:userId', userController.deleteUserById);

module.exports = router;