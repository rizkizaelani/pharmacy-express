const express = require('express');
const router = express.Router();

const inventoryController = require('../app/api/controllers/inventory');

router.get('/', inventoryController.getInventoryList);

module.exports = router;