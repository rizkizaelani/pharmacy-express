const express = require('express');
const router = express.Router();

const pharmacyController = require('../app/api/controllers/pharmacy');
const inventoryController = require('../app/api/controllers/inventory');
const searchController = require('../app/api/controllers/search')

router.post('/', pharmacyController.createPharmacy);
router.get('/', pharmacyController.getAllPharmacy);
router.get('/:pharmacyId', pharmacyController.getPharmacyById);
router.put('/:pharmacyId', pharmacyController.updatePharmacyById);
router.delete('/:pharmacyId', pharmacyController.deletePharmacyById);

// ROUTE INVENTORY PHARMACY
router.post('/:pharmacy_id/inventory', inventoryController.createInventory);
router.get('/:pharmacy_id/inventory', inventoryController.getAllInventory);
router.get('/:pharmacy_id/inventory/:inventory_id', inventoryController.getInventoryById);
router.put('/:pharmacy_id/inventory/:inventory_id', inventoryController.updateInventoryById);
router.delete('/:pharmacy_id/inventory/:inventory_id', inventoryController.deleteinventoryById);

// SEARCH MEDICINE NAME BY INVENTORY
router.post('/findNearestByMedicineName', searchController.findNearestByMedicineName);


module.exports = router;