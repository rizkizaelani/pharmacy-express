const express = require('express');
const router = express.Router();

const userController = require('../app/api/controllers/users');

router.post('/', userController.createUser);
router.post('/login', userController.login);

module.exports = router;