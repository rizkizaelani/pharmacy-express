const createError = require('http-errors');
const _ = require('lodash');

const medicineModel = require('../models/medicine');
const u = require('../../../libs/utils')

module.exports = {
    createMedicine: (req, res, next) => {

        medicineModel.create({
            name: (req.body.name) ? req.body.name : null,
            description: (req.body.description) ? req.body.description : null,
            category: (req.body.category) ? req.body.category : null,
            dosis: (req.body.dosis) ? req.body.dosis : null,
            village: (req.body.village) ? req.body.village : null,
            code: (req.body.code) ? req.body.code : null,
            expired_at: (req.body.expired_at) ? req.body.expired_at : null,
        }, (err, result) => {
            if (err) {
                console.log("POST createMedicine error", {
                    err: err
                });
                return next(
                    createError(500, "POST createMedicine error, err : " + err)
                );
            }
            return res.json(result);
        });
    },
    getAllMedicine: (req, res, next) => {

        var allowedKeys = {
            id: "_id",
            name: "name",
            category: "category",
            dosis: "dosis",
            code: "code",
            expired_at: "expired_at"
        };

        let filters = {};

        if (req.query.filter) {
            try {
                filters = u.transformFilter(req.query.filter, filters, allowedKeys);
            } catch (error) {
                console.log("Get medicine filter failed", {
                    params: {
                        query: req.query
                    },
                    err: error
                });

                return next(createError(400, "invalid query parameters", { code: "INVALID_QUERY_PARAMETERS" }))
            }
        }

        if (req.query.limit < 0) {
            return res.status(500).json({
                code: "",
                message: "",
            });
        }

        let query = u.createQueryParams(req, allowedKeys);

        medicineModel.countDocuments(filters, (err, totals) => {
            medicineModel
                .find(filters)
                .sort(query.sort)
                .skip(query.offset)
                .limit(query.limit)
                .lean()
                .exec((err, result) => {
                    if (err) {
                        console.log("GET medicine list error", {
                            err: err
                        });
                        return next(
                            createError(500, "GET medicine list error, err : " + err)
                        );
                    }

                    console.log("GET medicine list success", {
                        params: {
                            query: req.query
                        }
                    });

                    let toReturn = result.map(k => {
                        return {
                            ...k,
                            id: k._id
                        }
                    })

                    res.setHeader("X-Total-Count", totals);
                    toReturn = toReturn.map(obj => _.omit(obj, [
                        "_id",
                        "__v"
                    ]));
                    return res.json(toReturn);
                });
        });
    },
    getMedicineById: (req, res, next) => {
        medicineModel.findById(req.params.medicineId, (err, result) => {
            if (err) {
                console.log("GET /medicine/:medicineId error", {
                    params: {
                        id: req.params.medicineId
                    },
                    err: err
                });

                return next(
                    createError(500, "GET /medicine/:medicineId error, err : " + err)
                );
            }

            if (!result) {
                return next(
                    createError(404, "medicine not found", { code: "DATA_NOT_FOUND" })
                );
            }

            console.log("GET /medicine/:medicineId success", {
                params: {
                    id: req.params.medicineId
                }
            })

            return res.json(result);
        })
    },
    updateMedicineById: (req, res, next) => {
        const medicineUpdate = {
            name: (req.body.name) ? req.body.name : null,
            description: (req.body.description) ? req.body.description : null,
            category: (req.body.category) ? req.body.category : null,
            dosis: (req.body.dosis) ? req.body.dosis : null,
            village: (req.body.village) ? req.body.village : null,
            code: (req.body.code) ? req.body.code : null,
            expired_at: (req.body.expired_at) ? req.body.expired_at : null,
        }

        medicineModel.findOneAndUpdate(req.params.medicineId, medicineUpdate, (err, result) => {
            if (err) {
                console.log("GET /medicine/:medicineId error", {
                    params: {
                        id: req.params.medicineId
                    },
                    err: err
                });
                return next(
                    createError(500, "GET /medicine/:medicineId error, err : " + err)
                );
            }

            if (!result) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }

            let medicineResponse = {
                id: result._id,
                ...result._doc
            }

            medicineResponse = _.omit(medicineResponse, [
                "__v",
                "_id",
            ])

            return res.json(medicineResponse);
        });
    },
    deleteMedicineById: (req, res, next) => {
        medicineModel.findByIdAndDelete(req.params.medicineId, function (err, result) {
            if (err) {
                console.log("DELETE /medicine/:medicineId error", {
                    params: {
                        id: req.params.medicineId
                    },
                    err: err
                });
                return next(
                    createError(500, "DELETE /medicine/:medicineId error, err : " + err)
                );
            }

            if (!req.params.medicineId) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }
            return res.status(204).send()

        });
    }
}