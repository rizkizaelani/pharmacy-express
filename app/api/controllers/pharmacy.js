const createError = require('http-errors');
const _ = require('lodash');

const pharmacyModel = require('../models/pharmacy');
const u = require('../../../libs/utils')

module.exports = {
    createPharmacy: (req, res, next) => {

        pharmacyModel.create({
            name: (req.body.name) ? req.body.name : null,
            branch: (req.body.branch) ? req.body.branch : null,
            address: {
                street: (req.body.street) ? req.body.street : null,
                building: (req.body.building) ? req.body.building : null,
                village: (req.body.village) ? req.body.village : null,
                subdistrict: (req.body.subdistrict) ? req.body.subdistrict : null,
                city: (req.body.city) ? req.body.city : null,
                province: (req.body.province) ? req.body.province : null,
                zip_code: (req.body.zip_code) ? req.body.zip_code : null,
                state: (req.body.state) ? req.body.state : null
            },
            phone: (req.body.phone) ? req.body.phone : null,
            geolocation: {
                latitude: (req.body.latitude) ? req.body.latitude : null,
                longitude: (req.body.longitude) ? req.body.longitude : null
            },
            email: (req.body.email) ? req.body.phone : null
        }, (err, result) => {
            if (err) {
                console.log("POST createPharmacy error", {
                    err: err
                });
                return next(
                    createError(500, "POST createPharmacy error, err : " + err)
                );
            }
            return res.json(result);
        });
    },
    getAllPharmacy: (req, res, next) => {

        var allowedKeys = {
            id: "_id",
            name: "name",
            email: "email",
            phone: "phone",
            branch: "branch"
        };

        let filters = {};

        if (req.query.filter) {
            try {
                filters = u.transformFilter(req.query.filter, filters, allowedKeys);
            } catch (error) {
                console.log("Get pharmacy filter failed", {
                    params: {
                        query: req.query
                    },
                    err: error
                });

                return next(createError(400, "invalid query parameters", { code: "INVALID_QUERY_PARAMETERS" }))
            }
        }

        if (req.query.limit < 0) {
            return res.status(500).json({
                code: "",
                message: "",
            });
        }

        let query = u.createQueryParams(req, allowedKeys);

        pharmacyModel.countDocuments(filters, (err, totals) => {
            pharmacyModel
                .find(filters)
                .sort(query.sort)
                .skip(query.offset)
                .limit(query.limit)
                .lean()
                .exec((err, result) => {
                    if (err) {
                        console.log("GET pharmacy list error", {
                            err: err
                        });
                        return next(
                            createError(500, "GET pharmacy list error, err : " + err)
                        );
                    }

                    console.log("GET pharmacy list success", {
                        params: {
                            query: req.query
                        }
                    });

                    let toReturn = result.map(k => {
                        return {
                            ...k,
                            id: k._id
                        }
                    })

                    res.setHeader("X-Total-Count", totals);
                    toReturn = toReturn.map(obj => _.omit(obj, [
                        "_id",
                        "__v"
                    ]));
                    return res.json(toReturn);
                });
        });
    },
    getPharmacyById: (req, res, next) => {
        pharmacyModel.findById(req.params.pharmacyId, (err, result) => {
            if (err) {
                console.log("GET /pharmacy/:pharmacyId error", {
                    params: {
                        id: req.params.pharmacyId
                    },
                    err: err
                });

                return next(
                    createError(500, "GET /pharmacy/:pharmacyId error, err : " + err)
                );
            }

            if (!result) {
                return next(
                    createError(404, "pharmacy not found", { code: "DATA_NOT_FOUND" })
                );
            }

            console.log("GET /pharmacy/:pharmacyId success", {
                params: {
                    id: req.params.pharmacyId
                }
            })

            return res.json(result);
        })
    },
    updatePharmacyById: (req, res, next) => {
        const pharmacyUpdate = {
            name: (req.body.name) ? req.body.name : null,
            branch: (req.body.branch) ? req.body.branch : null,
            address: {
                street: (req.body.street) ? req.body.street : null,
                building: (req.body.building) ? req.body.building : null,
                village: (req.body.village) ? req.body.village : null,
                subdistrict: (req.body.subdistrict) ? req.body.subdistrict : null,
                city: (req.body.city) ? req.body.city : null,
                province: (req.body.province) ? req.body.province : null,
                zip_code: (req.body.zip_code) ? req.body.zip_code : null,
                state: (req.body.state) ? req.body.state : null
            },
            phone: (req.body.phone) ? req.body.phone : null,
            geolocation: {
                latitude: (req.body.latitude) ? req.body.latitude : null,
                longitude: (req.body.longitude) ? req.body.longitude : null
            },
            email: (req.body.email) ? req.body.phone : null
        }

        pharmacyModel.findOneAndUpdate(req.params.pharmacyId, pharmacyUpdate, (err, result) => {
            if (err) {
                console.log("GET /pharmacy/:pharmacyId error", {
                    params: {
                        id: req.params.pharmacyId
                    },
                    err: err
                });
                return next(
                    createError(500, "GET /pharmacy/:pharmacyId error, err : " + err)
                );
            }

            if (!result) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }

            let pharmacyResponse = {
                id: result._id,
                ...result._doc
            }

            pharmacyResponse = _.omit(pharmacyResponse, [
                "__v",
                "_id",
            ])

            return res.json(pharmacyResponse);
        });
    },
    deletePharmacyById: (req, res, next) => {
        pharmacyModel.findByIdAndDelete(req.params.pharmacyId, function (err, result) {
            if (err) {
                console.log("DELETE /pharmacy/:pharmacyId error", {
                    params: {
                        id: req.params.pharmacyId
                    },
                    err: err
                });
                return next(
                    createError(500, "DELETE /pharmacy/:pharmacyId error, err : " + err)
                );
            }

            if (!req.params.pharmacyId) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }
            return res.status(204).send()

        });
    }
}