const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const moment = require('moment');
const _ = require('lodash');

const userModel = require('../models/users');
const u = require('../../../libs/utils')

module.exports = {
    createUser: (req, res, next) => {

        req.body.created_at = moment().toDate();
        req.body.updated_at = moment().toDate();

        if (req.body.email) {
            checkEmail = userModel.find({ email: req.body.email }).then((result) => {
                return Promise.resolve(result.length > 0)
            });
        }

        console.log(req.body.email)

        checkEmail.then((emailExist) => {
            if (emailExist) {
                return res.status(400).json({
                    code: "EMAIL_EXISTS",
                    message: "EMAIL_EXISTS"
                });
            }

            userModel.create({
                firstname: (req.body.firstname) ? req.body.firstname : null,
                lastname: (req.body.lastname) ? req.body.lastname : null,
                email: (req.body.email) ? req.body.email : null,
                password: (req.body.password) ? req.body.password : null,
                phone_number: (req.body.phone_number) ? req.body.phone_number : null,
                latitude: (req.body.latitude) ? req.body.latitude : null,
                longitude: (req.body.longitude) ? req.body.longitude : null,
                street: (req.body.street) ? req.body.street : null,
                building: (req.body.building) ? req.body.building : null,
                village: (req.body.village) ? req.body.village : null,
                subdistrict: (req.body.subdistrict) ? req.body.subdistrict : null,
                city: (req.body.city) ? req.body.city : null,
                province: (req.body.province) ? req.body.province : null,
                zip_code: (req.body.zip_code) ? req.body.zip_code : null,
                state: (req.body.state) ? req.body.state : null,
                created_at: (req.body.created_at) ? req.body.created_at : null,
                updated_at: (req.body.updated_at) ? req.body.updated_at : null
            }, (err, result) => {
                if (err) {
                    console.log("POST createUser error", {
                        err: err
                    });
                    return next(
                        createError(500, "POST createUser error, err : " + err)
                    );
                }
                return res.json(result);
            });
        });

    },
    getAllUser: (req, res, next) => {

        var allowedKeys = {
            id: "_id",
            firstname: "firstname",
            lastname: "lastname",
            phone_number: "phone_number",
            email: "email"
        };

        let filters = {};

        if (req.query.filter) {
            try {
                filters = u.transformFilter(req.query.filter, filters, allowedKeys);
            } catch (error) {
                console.log("Get user filter failed", {
                    params: {
                        query: req.query
                    },
                    err: error
                });

                return next(createError(400, "invalid query parameters", { code: "INVALID_QUERY_PARAMETERS" }))
            }
        }

        if (req.query.limit < 0) {
            return res.status(500).json({
                code: "",
                message: "",
            });
        }

        let query = u.createQueryParams(req, allowedKeys);

        userModel.countDocuments(filters, (err, totals) => {
            userModel
                .find(filters)
                .sort(query.sort)
                .skip(query.offset)
                .limit(query.limit)
                .lean()
                .exec((err, result) => {
                    if (err) {
                        console.log("GET user list error", {
                            err: err
                        });
                        return next(
                            createError(500, "GET user list error, err : " + err)
                        );
                    }

                    console.log("GET user list success", {
                        params: {
                            query: req.query
                        }
                    });

                    let toReturn = result.map(k => {
                        return {
                            ...k
                        }
                    })

                    res.setHeader("X-Total-Count", totals);
                    toReturn = toReturn.map(obj => _.omit(obj, [
                        "password",
                        "_id",
                        "__v"
                    ]));
                    return res.json(toReturn);
                });
        });
    },
    getUserById: (req, res, next) => {

        userModel.findById(req.params.userId, (err, result) => {
            if (err) {
                console.log("GET /users/:userID error", {
                    params: {
                        id: req.params.userId
                    },
                    err: err
                });

                return next(
                    createError(500, "GET /users/:userId error, err : " + err)
                );
            }

            if (!result) {
                return next(
                    createError(404, "user not found", { code: "DATA_NOT_FOUND" })
                );
            }

            console.log("GET /users/:userId success", {
                params: {
                    id: req.params.userId
                }
            })

            return res.json(result);
        })
    },
    updateUserById: (req, res, next) => {

        const userUpdate = {
            firstname: (req.body.firstname) ? req.body.firstname : null,
            lastname: (req.body.lastname) ? req.body.lastname : null,
            email: (req.body.email) ? req.body.email : null,
            phone_number: (req.body.phone_number) ? req.body.phone_number : null,
            latitude: (req.body.latitude) ? req.body.latitude : null,
            longitude: (req.body.longitude) ? req.body.longitude : null,
            street: (req.body.street) ? req.body.street : null,
            building: (req.body.building) ? req.body.building : null,
            village: (req.body.village) ? req.body.village : null,
            subdistrict: (req.body.subdistrict) ? req.body.subdistrict : null,
            city: (req.body.city) ? req.body.city : null,
            province: (req.body.province) ? req.body.province : null,
            zip_code: (req.body.zip_code) ? req.body.zip_code : null,
            state: (req.body.state) ? req.body.state : null,
            created_at: (req.body.created_at) ? req.body.created_at : null,
            updated_at: (req.body.updated_at) ? req.body.updated_at : null
        }

        userModel.findOneAndUpdate(req.params.userId, userUpdate, (err, result) => {
            if (err) {
                console.log("GET /users/:userId error", {
                    params: {
                        id: req.params.userId
                    },
                    err: err
                });
                return next(
                    createError(500, "GET /users/:userId error, err : " + err)
                );
            }

            if (!result) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }

            let userResponse = {
                id: result._id,
                ...result._doc
            }

            userResponse = _.omit(userResponse, [
                "password",
                "__v",
                "_id",
            ])

            return res.json(userResponse);
        });
    },
    deleteUserById: (req, res, next) => {
        userModel.findByIdAndDelete(req.params.userId, function (err, result) {
            if (err) {
                console.log("DELETE /users/:userId error", {
                    params: {
                        id: req.params.userId
                    },
                    err: err
                });
                return next(
                    createError(500, "DELETE /users/:userId error, err : " + err)
                );
            }

            if (!req.params.userId) {
                return (
                    createError(404, "data not found", { code: "DATA_NOT_FOUND" })
                );
            }
            return res.status(204).send()

        });
    },
    login: (req, res, next) => {
        console.log("email", req.body.email)
        console.log("password", req.body.password)

        let checkUser = userModel.findOne({ email: req.body.email }, (err, result) => {
            if (err) {
                req.log.error("GET /users/login error", {
                    params: {
                        id: req.id
                    },
                    err: err
                });

                return next(
                    createError(500, "GET /users/login error, err : " + err)
                );
            }

            if (result) {
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false)
            }
        });

        checkUser.then((EmailExist) => {
            if (!EmailExist) {
                return res.status(400).json({
                    id: req.id,
                    code: 'USER_NOT_FOUND',
                    message: 'USER_NOT_FOUND',
                });
            } else {
                userModel.findOne(
                    {
                        email: req.body.email
                    }, (err, user) => {
                        if (err) {
                            next(err)
                        } else {
                            if (bcrypt.compareSync(req.body.password, user.password)) {
                                const token = jwt.sign(
                                    { user },
                                    req.app.get('secretKey'),
                                    { expiresIn: '365d' });

                                res.json({
                                    // data: user,
                                    token: token
                                });
                            } else {
                                return res.status(403).json({
                                    id: req.id,
                                    message: 'INVALID_EMAIL_OR_PASSWORD',
                                });
                            }
                        }
                    }
                );
            }
        });
    },
}