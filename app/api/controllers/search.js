const createError = require('http-errors');
const _ = require('lodash');
const async = require("async");

const inventoryModel = require('../models/inventory');
const pharmacyModel = require('../models/pharmacy');
const medicineModel = require('../models/medicine');

const u = require('../../../libs/utils')

module.exports = {
    findNearestByMedicineName: (req, res, next) => {

        console.log(req.body)

        medicineModel
            .find({ "name": { "$regex": req.body.keyword, "$options": "i" } })
            .exec((err, medicine) => {

                if (err) {
                    console.log("get Medicine by findNearestByMedicineName error", {
                        err: err
                    });
                    return next(
                        createError(500, "get Medicine by findNearestByMedicineName error, err : " + err)
                    );
                }

                console.log("medicine", medicine);
                console.log("length", medicine.length);

                let ids = medicine.map(x =>
                    x._id
                )

                console.log("medicine_id", ids)

                var obj_ids = ids.map((id) => {
                    return Object(id);
                });

                console.log("obj_ids", obj_ids)

                inventoryModel
                    .find({ "medicine": { $in: obj_ids } })
                    .populate([
                        {
                            path: "pharmacy"
                        },
                        {
                            path: "medicine"
                        }
                    ])
                    .exec((err, result) => {

                        if (err) {
                            console.log("findNearestByMedicineName error", {
                                err: err
                            });
                            return next(
                                createError(500, "findNearestByMedicineName error, err : " + err)
                            );
                        }

                        if (!result) {
                            console.log("No data found");
                            return res.json({ items: [] })
                        }

                        console.log("inventory", result)

                        return res.json({
                            status: true,
                            message: "success",
                            items: result
                        });
                    })
            })
    }
}