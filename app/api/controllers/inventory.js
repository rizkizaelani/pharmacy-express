const createError = require('http-errors');
const _ = require('lodash');
const async = require("async");

const inventoryModel = require('../models/inventory');
const pharmacyModel = require('../models/pharmacy');
const medicineModel = require('../models/medicine');

const u = require('../../../libs/utils')

module.exports = {
    createInventory: (req, res, next) => {

        async.parallel({
            pharmacy: function (callback) {
                pharmacyModel
                    .findOne({
                        _id: req.params.pharmacy_id
                    })
                    .exec((err, pharmacy) => {
                        callback(err, pharmacy);
                    })
            },
            medicine: function (callback) {
                medicineModel
                    .findOne({
                        _id: req.body.medicine
                    })
                    .exec((err, medicine) => {
                        callback(err, medicine);
                    })
            }
        }, (err, results) => {

            if (!results.pharmacy) {
                return next(createError(404, "pharmacy not found", { code: 404 }));
            }

            if (!results.medicine) {
                return next(createError(400, "medicine not found", { code: 400 }))
            }

            req.body.pharmacy = req.params.pharmacy_id;
            console.log(req.body)
            inventoryModel.create(
                req.body,
                (err, inventory) => {
                    if (err) {
                        console.log("POST /pharmacy/:pharmacy_id/medicine error", {
                            params: {
                                body: req.body
                            },
                            err: err
                        });
                        return next(createError(500, "POST /pharmacy/:pharmacy_id/medicine, err : " + err));
                    }

                    console.log("POST /pharmacy/:pharmacy_id/medicine success", {
                        params: {
                            body: req.body
                        }
                    });

                    // inventory.populate('pharmacy medicine', function (err) {
                    return res.status(201).json(inventory);
                    // });
                }
            )
        })
    },
    getAllInventory: (req, res, next) => {

        var allowedKeys = {
            "medicine.name": "medicine.name",
            "expired_at": "expired_at",
            "stock": "stock",
            "dosis": "price",
        };

        if (req.query.filter) {
            try {
                filters = u.transformFilter(req.query.filter, filters, allowedKeys);
            } catch (error) {
                console.log("Get inventory filter failed", {
                    params: {
                        query: req.query
                    },
                    err: error
                });
                return next(createError(400, "invalid query parameters", { code: "INVALID_QUERY_PARAMETERS" }))
            }
        }

        if (req.query.limit < 0) {
            return res.status(500).json({
                code: "",
                message: "",
            });
        }

        pharmacyModel
            .findOne({ _id: req.params.pharmacy_id })
            .exec((err, pharmacy) => {
                if (err) {
                    console.log("GET /pharmacy/:pharmacy_id/medicine error on findOne pharmacy", {
                        params: {
                            query: req.query
                        },
                        err: err
                    });

                    return next(createError(500, "GET /pharmacy/:pharmacy_id/medicine error, err : " + err));
                }

                if (!pharmacy) {
                    return next(createError(404, "pharmacy not found", { code: 404 }));
                }

                let query = u.createQueryParams(req, allowedKeys);

                let filters = {
                    pharmacy: req.params.pharmacy_id
                };

                inventoryModel.countDocuments(filters, (err, totals) => {

                    inventoryModel
                        .find(filters)
                        .sort(query.sort)
                        .skip(query.offset)
                        .limit(query.limit)
                        .populate([
                            {
                                path: "pharmacy"
                            },
                            {
                                path: "medicine"
                            }
                        ])
                        .exec((err, inventory) => {
                            console.log(inventory)
                            if (err) {
                                console.log("GET inventory list error", {
                                    err: err
                                });
                                return next(
                                    createError(500, "GET inventory list error, err : " + err)
                                );
                            }

                            console.log("GET inventory list success", {
                                params: {
                                    query: req.query
                                }
                            });

                            res.setHeader("X-Total-Count", totals);
                            return res.json(inventory)

                        });
                });
            });
    },
    getInventoryById: (req, res, next) => {
        pharmacyModel
            .findOne({ _id: req.params.pharmacy_id })
            .exec((err, pharmacy) => {
                if (err) {
                    console.log("GET /pharmacy/:pharmacy_id/inventory/:inventory_id on findOne error", {
                        params:
                        {
                            id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(500, "GET /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                }
                if (!pharmacy) {
                    console.log("GET /pharmacy/:pharmacy_id/inventory/:inventory_id findOne pharmacy error", {
                        params:
                        {
                            _id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(404, "pharmacy not found", { code: 404 }))
                }

                inventoryModel
                    .findOne({
                        _id: req.params.inventory_id,
                        pharmacy: req.params.pharmacy_id
                    })
                    .populate("pharmacy")
                    .populate("medicine")
                    .exec((err, inventory) => {
                        if (err) {
                            console.log("GET /pharmacy/:pharmacy_id/inventory/:inventory_id error", {
                                params:
                                {
                                    id: req.params.inventory_id
                                },
                                err: err
                            });
                            return next(createError(500, "GET /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                        }
                        if (!inventory) {
                            return next(createError(404, "inventory not found", { code: 404 }))
                        }
                        console.log("GET /pharmacy/:pharmacy_id/inventory/:inventory_id success", {
                            params: {
                                id: req.params.inventory_id
                            }
                        });

                        return res.json(inventory);
                    });
            });
    },
    updateInventoryById: (req, res, next) => {
        pharmacyModel
            .findOne({ _id: req.params.pharmacy_id })
            .exec((err, pharmacy) => {
                if (err) {
                    console.log("PUT /pharmacy/:pharmacy_id/inventory/:inventory_id on findOne error", {
                        params:
                        {
                            id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(500, "GET /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                }
                if (!pharmacy) {
                    console.log("PUT /pharmacy/:pharmacy_id/inventory/:inventory_id findOne pharmacy error", {
                        params:
                        {
                            _id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(404, "pharmacy not found", { code: 404 }))
                }

                inventoryModel
                    .findOneAndUpdate({
                        _id: req.params.inventory_id,
                        pharmacy: req.params.pharmacy_id
                    }, req.body, { new: true })
                    .exec((err, inventory) => {
                        if (err) {
                            console.log("PUT /pharmacy/:pharmacy_id/inventory/:inventory_id error", {
                                params:
                                {
                                    id: req.params.inventory_id,
                                    body: req.body
                                },
                                err: err
                            });
                            return next(createError(500, "PUT /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                        }

                        if (!inventory) {
                            return next(createError(404, "inventory not found", { code: 404 }))
                        }

                        console.log("PUT /pharmacy/:pharmacy_id/inventory/:inventory_id success", {
                            params: {
                                body: req.body,
                                id: req.params.inventory_id
                            }
                        });

                        return res.json(inventory);
                    })
            })
    },
    deleteinventoryById: (req, res, next) => {
        pharmacyModel
            .findOne({ _id: req.params.pharmacy_id })
            .exec((err, pharmacy) => {

                if (err) {
                    console.log("Delete /pharmacy/:pharmacy_id/inventory/:inventory_id on findOne error", {
                        params:
                        {
                            id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(500, "GET /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                }
                if (!pharmacy) {
                    console.log("Delete /pharmacy/:pharmacy_id/inventory/:inventory_id findOne pharmacy error", {
                        params:
                        {
                            _id: req.params.inventory_id
                        },
                        err: err
                    });
                    return next(createError(404, "pharmacy not found", { code: 404 }))
                }

                inventoryModel
                    .findOne({
                        _id: req.params.inventory_id,
                        pharmacy: req.params.pharmacy_id
                    })
                    .exec((err, inventory) => {

                        console.log("DELETE /pharmacy/:pharmacy_id/inventory/:inventory_id error", { err, inventory })
                        if (err) {
                            console.log("DELETE /pharmacy/:pharmacy_id/inventory/:inventory_id error", {
                                params: {

                                    id: req.params.id
                                },
                                err: err
                            });
                            return next(createError(500, "DELETE /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                        }

                        if (!inventory) {
                            return next(createError(404, "inventory not found or deleted", { code: 404 }))
                        }

                        inventory.remove({ _id: inventory._id })
                            .then(() => {
                                console.log("DELETE /pharmacy/:pharmacy_id/inventory/:inventory_id success", {
                                    params: {
                                        id: req.params.inventory_id
                                    }
                                });
                                return res.status(204).send("")
                            }).catch(err => {
                                return next(createError(500, "DELETE /pharmacy/:pharmacy_id/inventory/:inventory_id, err : " + err));
                            })
                    })
            })
    },
    getInventoryList: (req, res, next) => {
        var allowedKeys = {
            id: "_id",
            medicine_name: "medicine.name",
            pharmacy_name: "pharmacy.name",
            stock: "stock",
            price: "price",
        };

        let filters = {};

        if (req.query.filter) {
            try {
                filters = u.transformFilter(req.query.filter, filters, allowedKeys);
            } catch (error) {
                console.log("Get medicine filter failed", {
                    params: {
                        query: req.query
                    },
                    err: error
                });

                return next(createError(400, "invalid query parameters", { code: "INVALID_QUERY_PARAMETERS" }))
            }
        }

        if (req.query.limit < 0) {
            return res.status(500).json({
                code: "",
                message: "",
            });
        }

        let query = u.createQueryParams(req, allowedKeys);

        inventoryModel.countDocuments(filters, (err, totals) => {
            inventoryModel
                .find(filters)
                .sort(query.sort)
                .skip(query.offset)
                .limit(query.limit)
                .lean()
                .populate([
                    {
                        path: "pharmacy"
                    },
                    {
                        path: "medicine"
                    }
                ])
                .exec((err, result) => {
                    if (err) {
                        console.log("GET getInventoryList error", {
                            err: err
                        });
                        return next(
                            createError(500, "GET getInventoryList error, err : " + err)
                        );
                    }

                    console.log("GET getInventoryList success", {
                        params: {
                            query: req.query
                        }
                    });

                    let toReturn = result.map(k => {
                        return {
                            ...k,
                            id: k._id
                        }
                    })

                    res.setHeader("X-Total-Count", totals);
                    toReturn = toReturn.map(obj => _.omit(obj, [
                        "_id",
                        "__v"
                    ]));
                    return res.json(toReturn);
                });
        });
    }
}