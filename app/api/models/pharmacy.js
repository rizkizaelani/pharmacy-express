const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const uuid = require('uuid');

const pharmacySchema = new Schema({

    name: { type: String, required: true },
    branch: { type: String, required: true },
    email: { type: String, trim: true },
    phone: { type: String },
    address: {
        street: { type: String },
        building: { type: String },
        village: { type: String },
        subdistrict: { type: String },
        city: { type: String },
        province: { type: String },
        zip_code: { type: String },
        state: { type: String },
    },
    geolocation: {
        latitude: { type: Number },
        longitude: { type: Number },
    }

});

pharmacySchema.virtual('inventory', {
    ref: 'Inventory',
    localField: '_id',
    foreignField: 'pharmacy',
    justOne: false
});

module.exports = mongoose.model('Pharmacy', pharmacySchema);