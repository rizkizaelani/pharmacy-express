const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const uuid = require('uuid');

const inventorySchema = new Schema(
    {
        pharmacy: { type: String, ref: "Pharmacy" },
        medicine: { type: String, ref: "Medicine" },
        price: { type: String, required: true },
        stock: { type: String, required: true },
        created_at: { type: Schema.Types.Date, default: Date.now },
        updated_at: { type: Schema.Types.Date, default: Date.now },
        expired_at: { type: String }
    }, {
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true 
        }
    }
);

inventorySchema.pre("findOneAndUpdate", function () {
    this.update({}, { $set: { updated_at: new Date() } });
});

// inventorySchema.virtual('pharmacy', {
//     ref: 'Pharmacy',
//     localField: '_id',
//     foreignField: 'inventory',
//     justOne: false
//   });

module.exports = mongoose.model('Inventory', inventorySchema);