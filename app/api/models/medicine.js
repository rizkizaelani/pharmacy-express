const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const uuid = require('uuid');

const medicineSchema = new Schema({

    name: { type: String, required: true },
    description: { type: String, required: true },
    category: { type: String, required: true },
    dosis: { type: String, required: true },
    code: { type: String, required: true },
    expired_at: { type: String }

});

// hash user password before saving into database
// pharmacySchema.pre('save', function (next) {
//     next();
// });

medicineSchema.virtual('inventory', {
    ref: 'Inventory',
    localField: '_id',
    foreignField: 'medicine',
    justOne: false
});

module.exports = mongoose.model('Medicine', medicineSchema);