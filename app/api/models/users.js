const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
//Define a schema
const Schema = mongoose.Schema;

const moment = require('moment');
const uuid = require('uuid');

const saltRounds = 10;

const UserSchema = new Schema({
    _id: { type: String, default: uuid.v4 },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, trim: true, required: true },
    password: { type: String, required: true },
    created_at: { type: Schema.Types.Date, default: Date.now },
    updated_at: { type: Schema.Types.Date, default: Date.now },
    phone_number: { type: String },
    latitude: { type: Number },
    longitude: { type: Number },
    street: { type: String },
    building: { type: String },
    village: { type: String },
    subdistrict: { type: String },
    city: { type: String },
    province: { type: String },
    zip_code: { type: String },
    state: { type: String }
});

// hash user password before saving into database
UserSchema.pre('save', function (next) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
    this.update({}, { $set: { updated_at: new Date() } });
    next();
});

module.exports = mongoose.model('User', UserSchema);