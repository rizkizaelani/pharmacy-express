const lodash = require("lodash");
const createError = require("http-errors");

const detectType = v => {
    return /^[1-9]([0-9]*)$/.test(v) ?
        parseInt(v) :
        /^[1-9]([0-9]*)\.([0-9]+)$/.test(v) ? parseFloat(v) : String(v);
};

const transformFilter = (queryFilter, defaultFilter, allowedKeys) => {
    var d = detectType;
    var ops = {
        "=": v => {
            return { $eq: v };
        },
        "!=": v => {
            return { $ne: v };
        },
        ">": v => {
            return { $gt: v };
        },
        ">=": v => {
            return { $gte: v };
        },
        "<": v => {
            return { $lt: v };
        },
        "<=": v => {
            return { $lte: v };
        },
        contains: v => {
            return { $regex: String(v), $options: "i" };
        },

        begins: v => {
            return lodash.isArray(v) ?
                { $in: v } :
                { $regex: "^" + v + "", $options: "i" };
        },

        ends: v => {
            return lodash.isArray(v) ?
                { $in: v } :
                { $regex: v + "$", $options: "i" };
        },
        not_contains: v => {
            return lodash.isArray(v) ?
                { $nin: v } :
                { $regex: "", $options: "i", $nin: [v] };
        },
        in: v => {
            return {
                $in: v.split(",").filter(x => {
                    return !lodash.isEmpty(x);
                })
            };
        },
        not_in: v => {
            return {
                $nin: v.split(",").filter(x => {
                    return !lodash.isEmpty(x);
                })
            };
        }
    };

    return lodash.reduce(
        lodash.map(lodash.isArray(queryFilter) ? queryFilter : [queryFilter], s => {
            return s.split("|");
        }),
        (c, i) => {
            if (
                i.length != 3 ||
                !lodash.has(allowedKeys, i[0]) ||
                !lodash.has(ops, i[1])
            ) {
                throw "Invalid filter";
            }

            let key = allowedKeys[i[0]];

            if (typeof allowedKeys[i[0]] === "object") {
                if (allowedKeys[i[0]].type && allowedKeys[i[0]].type === "boolean") {

                    if (
                        lodash.indexOf(["=", "!="], i[1]) != -1 &&
                        lodash.indexOf(["true", "false"], i[2]) != -1
                    ) {

                        if (i[2] === "false") {
                            if (i[1] === "=") {
                                i[1] = "!=";
                                i[2] = true;
                            } else if (i[1] === "!=") {
                                i[1] = "=";
                                i[2] = true;
                            }
                        }
                    } else {
                        throw "Invalid boolean filter"
                    }
                    key = allowedKeys[i[0]].property;
                }
            }

            c[key] = lodash.assign(
                c[key],
                ops[i[1]](d(i[2]))
            );
            return c;
        },
        defaultFilter
    );
};

const createQueryParams = (req, allowedKeys) => {
    let query = {};
    let sort =
        req.query.sort == null || !lodash.has(allowedKeys, req.query.sort) ?
            "created_at" :
            allowedKeys[req.query.sort],
        direction = req.query.direction == "desc" ? "-" : "",
        offset = req.query.offset == null ? 0 : parseInt(req.query.offset),
        limit =
            req.query.limit == null ?
                10 :
                req.query.limit == -1 ? null : parseInt(req.query.limit);

    return (query = { sort: direction + sort, offset: offset, limit: limit });
};

module.exports = {
    createQueryParams,
    transformFilter,
    detectType
}